import 'package:blocapp/bloc/get_sources_bloc.dart';
import 'package:blocapp/elements/error_element.dart';
import 'package:blocapp/elements/loader_element.dart';
import 'package:blocapp/model/source.dart';
import 'package:blocapp/model/source_response.dart';
import 'package:blocapp/screens/source_details.dart';
import 'package:flutter/material.dart';

class SourcesScreen extends StatefulWidget {
  @override
  _SourcesScreenState createState() => _SourcesScreenState();
}

class _SourcesScreenState extends State<SourcesScreen> {
  @override
  void initState() {
    super.initState();
    getSourcesBloc..getSources();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<SourceResponse>(
      stream: getSourcesBloc.subject.stream,
      builder: (context, AsyncSnapshot<SourceResponse> snapshot) {
        if (snapshot.hasData) {
          if (snapshot.data.error != null && snapshot.data.error.length > 0) {
            return getErrorWidget(snapshot.data.error);
          }
          return _buildSources(snapshot.data);
        } else if (snapshot.hasError) {
          return getErrorWidget(snapshot.error);
        } else {
          return buildLoadingWidget();
        }
      },
    );
  }

  _buildSources(SourceResponse data) {
    List<SourceModel> sources = data.sources;
    return GridView.builder(
      itemCount: sources.length,
      gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
        crossAxisCount: 3,
        childAspectRatio: 0.86,
      ),
      itemBuilder: (c, p) {
        return Padding(
          padding: EdgeInsets.only(
            top: 10.0,
            left: 5.0,
            right: 5.0,
          ),
          child: GestureDetector(
            onTap: () {
              Navigator.push(context, MaterialPageRoute(builder: (c) {
                return SourceDetails(source: sources[p]);
              }));
            },
            child: Container(
              width: 100.0,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.all(
                  Radius.circular(5.0),
                ),
                boxShadow: [
                  BoxShadow(
                      color: Colors.grey[100],
                      blurRadius: 5.0,
                      spreadRadius: 1.0,
                      offset: Offset(1.0, 1.0))
                ],
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Hero(
                    tag: sources[p].id,
                    child: Container(
                      height: 60.0,
                      width: 60.0,
                      decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image:
                              AssetImage('assets/logos/${sources[p].id}.png'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Container(
                    padding: EdgeInsets.symmetric(
                      vertical: 15.0,
                      horizontal: 10.0,
                    ),
                    child: Text(
                      sources[p].name,
                      textAlign: TextAlign.center,
                      maxLines: 2,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 12.0,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
        );
      },
    );
  }
}
