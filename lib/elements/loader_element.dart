import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';

Widget buildLoadingWidget() {
  return Center(
    child: Column(
      children: [CupertinoActivityIndicator()],
    ),
  );
}
