import 'package:flutter/material.dart';

Widget getErrorWidget(String error) {
  return Container(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: [
        Text(
          error,
          style: TextStyle(fontSize: 12.0, fontWeight: FontWeight.bold),
        )
      ],
    ),
  );
}
