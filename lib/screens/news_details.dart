import 'package:blocapp/style/theme.dart';
import 'package:flutter/material.dart';
import 'package:blocapp/model/article.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';

class NewsDetails extends StatefulWidget {
  final ArticleModel article;
  NewsDetails(this.article);
  @override
  _NewsDetailsState createState() => _NewsDetailsState(this.article);
}

class _NewsDetailsState extends State<NewsDetails> {
  final ArticleModel article;
  _NewsDetailsState(this.article);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: GestureDetector(
        onTap: () {
          launch(article.url);
        },
        child: Container(
          height: 48.0,
          color: MyColors.mainColor,
          width: MediaQuery.of(context).size.width,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Read more",
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 15.0,
                ),
              )
            ],
          ),
        ),
      ),
      appBar: AppBar(
        elevation: 0.0,
        backgroundColor: MyColors.mainColor,
        title: Text(
          article.title,
          style: TextStyle(
            color: Colors.white,
            fontSize: 14.0,
            fontWeight: FontWeight.bold,
          ),
        ),
      ),
      body: ListView(
        children: [
          AspectRatio(
            aspectRatio: 16 / 9,
            child: FadeInImage.assetNetwork(
              placeholder: 'asset/img/placeholder.jpg',
              image: article.img,
              fit: BoxFit.cover,
            ),
          ),
          Container(
            padding: EdgeInsets.all(10.0),
            child: Column(
              children: [
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  article.date.substring(0, 10),
                  style: TextStyle(
                      color: MyColors.mainColor, fontWeight: FontWeight.bold),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Text(
                  article.title,
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    color: Colors.black,
                  ),
                ),
                SizedBox(
                  height: 10.0,
                ),
                Html(data: article.content),
              ],
            ),
          )
        ],
      ),
    );
  }
}
