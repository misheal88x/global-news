import 'package:flutter/cupertino.dart';

class MyColors {
  const MyColors();

  static const Color mainColor = Color(0xfff6511D);
  static const Color grey = Color(0xffE5E5E5);
}
